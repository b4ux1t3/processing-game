import * as p5 from "p5";
import { Game } from "./Game";



/*
Represents the "sketch" of our game. This is basically the "entry point" to our p5.js
application, similar to when you have just a screen with the setup and draw functions.
*/
export function newSketch(sketch: p5): void {
  const SCREEN_WIDTH  = 400;
  const SCREEN_HEIGHT = 400;
  // Represents our actual game state.
  // Since p5.js is weird, we're passing a reference to our created p5.js sketch,
  // allowing the game to use it to do things like render objects. More info below on draw.
  const game = new Game(sketch, SCREEN_WIDTH, SCREEN_HEIGHT);

  // We shouldn't do too much in setup, sicne most of the state is managed by our Game class.
  sketch.setup = () => {
    sketch.createCanvas(SCREEN_WIDTH, SCREEN_HEIGHT);
  }

  // We'll let our game choose what to draw and when. Since we pass our sketch down to
  // the game in its constructor, we can control what the screen looks like directly from
  // our game state.
  sketch.draw = () => {
    game.Draw();
  }
}
