import * as p5 from "p5";
import { HasPosition } from "./common/QuadTree";

/*
  This is basically a test class to prove out the concept of having objects that can
  draw themselves.
*/
export class Point implements HasPosition{
  private position: p5.Vector;

  constructor(x: number, y: number, private sketch: p5) {
    this.position = new p5.Vector();
    this.position.x = x;
    this.position.y = y;
  }

  public get x(): number {
    return this.position.x;
  }

  public get y(): number {
    return this.position.y;
  }

  public get vec(): p5.Vector {
    return this.position;
  }

  Draw(): void {
    this.sketch.strokeWeight(5);
    this.sketch.point(this.position.x, this.position.y);
  }
}
