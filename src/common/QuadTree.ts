import * as p5 from 'p5';

export class QuadTree<T extends HasPosition>  {
  /* Public properties */
  member: T;
  subdivisions: QuadTree<T>[];

  /* Private Properties */
  private divided = false;

  /* Public Methods */
  constructor(public boundary: Boundary, private sketch: p5 = null) {
    this.member = null;
  }

  Insert(child: T): void {
    // short circuit if the child isn't even in our boundary. There's no reason to take any action.
    if (!this.InBoundary(child)) return;
    if (this.member !== null){
      this.Subdivide();
    }
    if (this.divided) {
      this.subdivisions.forEach((subQuadTree: QuadTree<T>) => {
        subQuadTree.Insert(child);
      });
      return;
    }

    this.member = child;
  }

  Draw(): void {
    if (this.sketch !== null){
      this.sketch.rect(this.boundary.x, this.boundary.y, this.boundary.w, this.boundary.h);
      if (this.subdivisions) this.subdivisions.forEach(qt => qt.Draw());
    }
  }

  Query(range: Boundary): T[]{
    if (!this.boundary.IntersectsBoundary(range)) return [];
    if (this.divided) {
      let returnArray: T[] = [];
      this.subdivisions.forEach((subQuadTree: QuadTree<T>) => {
        returnArray = [...returnArray, ...subQuadTree.Query(range)];
      })
      return returnArray.filter((item: T) => item !== null);
    }
    return [this.member];
  }

  /* Private Methods */
  private Subdivide(): void {
    if (this.divided) return; // short circuit if we're already divided.
    this.subdivisions = [];
    this.divided = true;
    for (let i = 0; i < 4; i++){
      this.subdivisions[i] = new QuadTree<T>(this.CalcBoundary(i), this.sketch);
      this.subdivisions[i].Insert(this.member);
    }
    this.member = null;
  }
  // We'll divide our quadtree like so:
  // NW | NE    0 | 1
  // ------- => -----
  // SW | SE    3 | 2
  private CalcBoundary(index: number): Boundary {
    const x = this.boundary.x;
    const y = this.boundary.y;
    const w = this.boundary.w;
    const h = this.boundary.h;
    switch (index) {
      case 0: // NW
        return new Boundary(x, y, w / 2, h / 2);
      case 1: // NE
        return new Boundary(x + w / 2, y, w / 2, h / 2);
      case 2: // SE
        return new Boundary(x + w / 2, y + h / 2, w / 2, h / 2);
      case 3: // SW
        return new Boundary(x, y + w / 2, w / 2, h / 2);
    }
  }

  private InBoundary(child: T): boolean {
    const x = this.boundary.x;
    const y = this.boundary.y;
    const w = this.boundary.w;
    const h = this.boundary.h;
    // we need to be inclusive on one end and exclusive on the other.
    return (child.x < x + w && child.x >= x) &&
           (child.y < y + h && child.y >= y);
  }
  /* End Private Methods */
}

// represents the rectangle in which we track our point.
export class Boundary {

  constructor(public x: number, public y: number, public w: number, public h: number) {

  }

  IntersectsBoundary(other: Boundary): boolean {
    return !(                         // Invert the check of being outside other
      (this.x > other.x + other.w) || // Our left side is past other's right side
      (this.x + this.w  < other.x) || // Our right side is past other's left side
      (this.y > other.y + other.h) || // Our top is under other's bottom
      (this.y + this.h  < other.y)    // Our bottom is over other's top
    );
  }
}

export interface HasPosition {
  x: number;
  y: number;
  vec: p5.Vector;
}


