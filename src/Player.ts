import { Vector } from "p5";
import { Boundary, HasPosition } from "./common/QuadTree";

export class Player implements HasPosition{
  private position: Vector;
  public SearchSpace: Boundary;

  constructor(private searchRadius: number){
    this.position = new Vector();
  }

  get x(): number {
    return this.position.x;
  }
  get y(): number {
    return this.position.y;
  }
  get vec(): Vector {
    return this.position;
  }

  get SearchRadius(): number {
    return this.searchRadius;
  }

  UpdatePlayer(x: number, y: number): void {
    this.position.x = x;
    this.position.y = y;
    this.SearchSpace = new Boundary(
                                      this.x - this.searchRadius,
                                      this.y - this.searchRadius,
                                      this.searchRadius * 2,
                                      this.searchRadius * 2);


  }

  EatStuff<T extends HasPosition>(things: T[]): T[] {
    return things.filter((thing: T) => {
      return Vector.dist(this.position, thing.vec) < this.searchRadius;
    });
  }


}
