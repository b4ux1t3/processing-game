import * as p5 from "p5";
import { Boundary, QuadTree } from "./common/QuadTree";
import { Player } from "./Player";
import { Point } from "./Point";

export class Game {

  points: Point[] = [];
  player: Player;
  qt: QuadTree<Point>;
  private readonly playerSize = 10;

  constructor(private sketch: p5, private width: number, private height: number) {
    this.qt = new QuadTree<Point>(new Boundary(0, 0, width, height), sketch);
    for (let i = 0; i < 10; i++) {
      const x = sketch.randomGaussian(width / 2, width / 8);
      const y = sketch.randomGaussian(height / 2, height / 8);
      const newPoint = new Point(x, y, sketch);
      this.points.push(newPoint);
      this.qt.Insert(newPoint);
    }
    this.player = new Player(25);
    this.updatePlayer();
    this.sketch.mouseClicked = () => {
      const query = this.qt.Query(this.player.SearchSpace);
      const eatenThings = this.player.EatStuff(query);
      console.log('All the things in the rectangle:');
      console.log(query);
      console.log('All the eaten things:');
      console.log(eatenThings);
    };
  }

  // This is basically the equivalent of the native "draw" call in a normal p5.js sketch.
  // We'll choose what to render, where to render it, etc. all right here.
  // This method gets called by our sketch whenever it draws, so it's going to need to stay
  // pretty performant!
  public Draw(): void {
    this.updatePlayer();
    this.sketch.background(0, 127, 127);

    // For now, we're drawing a bunch of points, and I've gone ahead and implemented a draw
    // method for our points. We probably won't have to do that for everything.
    this.sketch.stroke(0);
    this.sketch.strokeWeight(3);
    this.points.forEach(point => point.Draw());

    // Next, draw the grid
    this.sketch.fill(0, 0, 0, 0)
    this.sketch.strokeWeight(1);
    this.qt.Draw(); // uncomment this if you want to look at the quadtree structure!

    // Draw the player's search boundary and the actual search area.
    // this.sketch.stroke(0, 255, 0);
    // this.sketch.rect(this.player.SearchSpace.x, this.player.SearchSpace.y, this.player.SearchSpace.w, this.player.SearchSpace.h);
    // this.sketch.stroke(255, 0, 0);
    // this.sketch.ellipse(this.player.x, this.player.y, this.player.SearchRadius * 2); // Ellipses are defined by diameter, not radius!

    // Draw lines to all of the points in our search space.
    this.sketch.stroke(0, 0, 255);
    const query: Point[] = this.qt.Query(this.player.SearchSpace);
    query.forEach((point: Point) => {
      if (p5.Vector.dist(this.player.vec, point.vec) < this.player.SearchRadius) this.sketch.stroke(0, 255, 0);
      else this.sketch.stroke(255, 0, 0);
      this.sketch.line(this.player.x, this.player.y, point.x, point.y);
    });

    // Draw the player
    this.sketch.stroke(0, 0, 0);
    this.sketch.fill(127, 127, 127);
    this.sketch.ellipse(this.player.x, this.player.y, this.playerSize);



  }

  private updatePlayer(): void {
    this.player.UpdatePlayer(this.sketch.mouseX, this.sketch.mouseY);
  }


}
