FROM node:latest AS build
WORKDIR /build
COPY . .

RUN npm ci
RUN npm run build-prod

FROM nginx:latest

COPY --from=build /build/dist/* /usr/share/nginx/html
EXPOSE 80
