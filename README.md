# Processing Game

### _(working title)_

This game is mostly to prove out that it is possible to sanely create [p5.js](https://p5js.org/) sketches with TypeScript. It does not export any functionality.

I don't know if this is the best way to do things but it's the way I'm doing it!

## How to Play

You can play the game [right here](https://b4ux1t3.gitlab.io/processing-game) on GitLab. There isn't much to it right now!

## How to Run It Locally

Under the hood, and besides p5.js, I'm using very common technologies:

* TypeScript, to give us a nice, robust type system, as any good game needs!
* `webpack`, in order to build and bundle everything.
* `webpack-dev-server`, which is, well, a development server!

So, if you want to work with this game locally (including just to play it), all you have to do is clone this repository (`git clone https://gitlab.com/b4ux1t3/processing-game`), navigate into the directory that is produced, and run the following commands:
```shell
npm ci # Install the dependencies. I build a package-lock file, so you don't have to do that part!
npm run watch # Spin up the development server.
```

That's all! The game should be playable on your local machine at `http://localhost:9000`.

### Docker

There is a `Dockerfile` which you can build using the command:

```shell
docker build -t whatevertagyouwant .
```

and then run with:

```shell
docker run -p 80:80 whatevertagyouwant
```

This will set up the docker container to listen on port 80. You should be able to browse to `http://localhost` and see it.

Alternatively, you can run it in Docker compose:

```shell
docker compose up -d --build
```

Because of, er, *reasons*, compose exposes port 5000, so you should be able to see the game at `http://localhost:5000`.
